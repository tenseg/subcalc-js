# SubCalc JS

SubCalc JS is the JavaScript project at the heart of the Minnesota Subcaucus Calculator. This project is designed to create a website that has essentially the same functionality as the [SubCalc iPhone ](https://itunes.apple.com/us/app/subcalc/id352454097?mt=8).

SubCalc assists convenors of precinct caucuses and conventions in Minnesota. The Minnesota Democratic Farmer Labor (DFL) party uses a wonderful, but bit arcane, “walking subcaucus” process that is simple enough to do, but rather difficult to tabulate. This app calculates the number of delegates each subcaucus gets when you enter the total number of delegates your precinct or convention is allowed and how many people are in each subcaucus. The rules it follows appeared on page 4 of the [DFL 2014 Official Call](http://www.sd64dfl.org/more/caucus2014printing/2014-Official-Call.pdf), including the proper treatment of remainders. It makes the math involved in a walking subcaucus disappear. The app could be used to facilitate a “walking subcaucus” or “[proportional representation](http://en.wikipedia.org/wiki/Proportional_representation)” system for any group.

## How to use SubCalc

When starting a new caucus or convention you can create a single subcaucus and just put the count of people in the room into that one subcaucus. The summary text will then tell you the viability number you need to announce to the room so they can gather the right number of supporters to their causes.

You don’t actually have to delete non-viable subcaucuses. If you just make the count of people in the subcaucus zero, you will still have a record that the subcaucus existed, but it won’t affect the distribution of delegates or the viability numbers at all.

Don’t forget to use the email function (upper right corner of the SubCalc view) to send yourself or others a record of what happened at the meeting. This is a great way to report your results.

When two or more subcaucuses have the same number of members, SubCalc will automatically do a “coin flip” to determine which, if any, should get remainder delegates. If someone insists on redoing this coin flip, you can tap the “flip the coin again” option below the summary. Remember, sometimes the new flip will have the exact same result as the old flip, so don’t be surprised if nothing changes. SubCalc will scroll you to the top of the subcaucus list as confirmation that it did, indeed, reflip the coin.

Randomness is a funny thing on a computer. While in the real world we would get together the leaders of tied subcaucuses and ask them to yell heads or tails while we flip a coin, we don’t actually have that opportunity in a computer program. SubCalc won’t hear anyone yelling heads or tails. What we really want, though, is simply a random decision about which of the tied subcaucuses will get remainder delegates. We’ve come up with a very different process in SubCalc, but we still call it a “coin flip” so that people understand it serves the same role.

In SubCalc we actually assign every subcaucus what we call a “randomRank” right when the subcaucus is created. Imagine this as asking each subcaucus leader to draw a straw as soon as they name their subcaucus. Each straw is a different length, and the subcaucus with the longest straw will get the first remainder in the event of a tie with another subcaucus. See, no “coin flips” to speak of, just a pre-assigned randomRank (straw) handed out at the beginning.

When you ask SubCalc to “flip the coin again” it actually just assigns new randomRanks to each subcaucus. This is as if you had asked each one to draw a new straw. Sometimes that results in the same order, sometimes the order of the randomRanks (the straws) will change. This is much like a coin flip, sometimes it comes up heads again, sometimes not.

We save the randomRanks with other data about each subcaucus so that when you quit SubCalc (or get a phone call on your iPhone, interrupting SubCalc) it won’t forget the order of the “straws” for the next time it has to calculate the results.